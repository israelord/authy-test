from phonenumber_field.modelfields import PhoneNumberField

from django.db import models
from django.contrib.auth.models import User


class AuthyProfile(models.Model):
    authy_id = models.IntegerField()
    user = models.OneToOneField(User)
    phone_number = PhoneNumberField()
    verified = models.BooleanField(default=False)
