from authy.api import AuthyApiClient

from phonenumber_field.formfields import PhoneNumberField

from django.conf import settings
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from auth_test.models import AuthyProfile

class RegistrationForm(UserCreationForm):
    """ This iherits from django's user registration
    form to use a subset of the fields so registration
    is simpler for the demo
    """
    email = forms.CharField(required=True, help_text='Required')

    class Meta:
        """ We override the class' Meta to tell it to
        use only a subset of fields
        """
        model = User
        fields = (
            'username',
            'email',
            'password1',
            'password2',
        )


class AuthyForm(forms.ModelForm):
    phone_number = PhoneNumberField(help_text='Ex. +971551112233')

    class Meta:
        model = AuthyProfile
        fields = ('phone_number',)

    def save(self, user, *args, **kwargs):
        authy_api = AuthyApiClient(settings.AUTHY_API_KEY)
        phone = self.cleaned_data['phone_number']
        authy_user = authy_api.users.create(
            user.email,
            phone.national_number,
            phone.country_code,
        )
        if authy_user.ok():
            AuthyProfile.objects.create(
                authy_id=authy_user.id,
                user=user,
                phone_number=phone.as_e164
            )
            authy_api.users.request_sms(authy_user.id)
            return True

        self.authy_errors = authy_user.errors()
        return False


class AuthyVerifyForm(forms.Form):
    token = forms.CharField()

    def process(self, user):
        authy_id = user.authyprofile.authy_id
        authy_api = AuthyApiClient(settings.AUTHY_API_KEY)
        try:
            verification = authy_api.tokens.verify(authy_id, self.cleaned_data['token'])
        except:
            return False

        user.authyprofile.verified = verification.ok()
        user.authyprofile.save()
        return verification.ok()
