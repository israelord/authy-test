"""auth_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.contrib.auth.decorators import login_required

import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'login/$', login, {'template_name': 'auth/login.html'}, name='login' ),
    url(r'logout/$', logout, {'next_page': '/login/'}, name='logout'),
    url(r'accounts/profile/$', login_required(views.ProfileView.as_view()), name='profile'),
    url(r'accounts/profile/auth/verify/$', login_required(views.AuthyVerifyView.as_view()), name='authy_verify'),
    url(r'authy/verify/$', login_required(views.VerifyAuthy.as_view()), name='verification'),
    url(r'^$', views.RegistrationView.as_view(), name='registration'),
]
