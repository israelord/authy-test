from django.conf import settings
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy as reverse
from django.contrib import messages
from django.shortcuts import render
from django.views import View

from .forms import RegistrationForm
from .forms import AuthyForm 
from .forms import AuthyVerifyForm


class RegistrationView(FormView):
    """ This handles the forms automatically based on
    the rules defined on the form_class attribute
    """
    template_name = 'auth/registration.html'
    form_class = RegistrationForm
    success_url = reverse('login')

    def form_valid(self, form):
        super(RegistrationView, self).form_valid(form)
        form.save()

        return HttpResponseRedirect(self.success_url)

class ProfileView(FormView):
    template_name = 'auth/profile.html'
    form_class = AuthyForm
    success_url = reverse('authy_verify')
    
    def get_context_data(self, *args, **kwargs):
        context = super(ProfileView, self).get_context_data(*args, **kwargs)
        user = self.request.user
        context['authy_verified'] = hasattr(user, 'authyprofile')
        return context

    def form_valid(self, form):
        saved = form.save(user=self.request.user)
        if saved:
            return HttpResponseRedirect(self.success_url)

        return HttpResponse(form.authy_errors, status=412)

    def form_invalid(self, form):
        return HttpResponse(form.errors, status=400)

    def dispatch(self, *args, **kwargs):
        user = self.request.user
        if hasattr(user, 'authyprofile') and not user.authyprofile.verified:
            return HttpResponseRedirect(reverse('logout'))

        return super(ProfileView, self).dispatch(*args, **kwargs)


class AuthyVerifyView(FormView):
    template_name = 'auth/authy_verify.html'
    form_class = AuthyVerifyForm
    success_url = reverse('profile')

    def form_valid(self, form):
        verified = form.process(user=self.request.user)

        if verified:
            return HttpResponseRedirect(self.success_url)

        messages.error(self.request, 'Invalid token, try again')
        return render(self.request, self.template_name, {'form': self.form_class()})

class VerifyAuthy(View):

    def get(self, *args, **kwargs):
        user = self.request.user

        if hasattr(user, 'authyprofile'):
            user.authyprofile.verified = False
            user.authyprofile.save()
            return HttpResponseRedirect(reverse('authy_verify'))

        return HttpResponseRedirect(reverse('profile'))
