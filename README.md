# How to run it?
Just clone the repo normally, you won't need to install any python dependencies but you'll need to have docker and docker-compose in order to run this
test.

Here is a good article on how to install them: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-14-04

Once you cloned the repo you only need to `cd` into the project's directory and run `docker-compose up`

It will create a container for the app, another one for the database, install all the dependencies, connect them together and run the test server.

# How to test it

Once it runs, you'll have to fire up your browser and go to `127.0.0.1:8000`

You'll be presented with a simple registration form. After that you'll have to login and setup MFA as requested.

Once you setup MFA with Authy, you'll be asked to enter a token eveytime after login. If you try to go to a different url, you will be logged out.
