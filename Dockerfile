FROM python:2
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN apt-get update -y
RUN apt-get install -y python-dev postgresql-server-dev-all
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
